#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/by-name/recovery$(getprop ro.boot.slot_suffix):69009408:c8f123faf6ca9733df8499b0c29f55b3e09d0aca; then
  applypatch \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/by-name/boot$(getprop ro.boot.slot_suffix):61865984:2a3804ec80b419f601777452ba7bde11ecba1349 \
          --target EMMC:/dev/block/by-name/recovery$(getprop ro.boot.slot_suffix):69009408:c8f123faf6ca9733df8499b0c29f55b3e09d0aca && \
      (/vendor/bin/log -t install_recovery "Installing new recovery image: succeeded" && setprop vendor.ota.recovery.status 200) || \
      (/vendor/bin/log -t install_recovery "Installing new recovery image: failed" && setprop vendor.ota.recovery.status 454)
else
  /vendor/bin/log -t install_recovery "Recovery image already installed" && setprop vendor.ota.recovery.status 200
fi

